﻿using Operations.Interfaces;
using System;

namespace Operations
{
    class OperationManager : IOperationManager
    {
        public event EventHandler<OperationEventArgs> OperationWasChosen;
        private IOperation[] _operations;
        private bool isOn;
        public delegate int TextHandler(string text);

        public OperationManager()
        {
            //Should be more but I didn't have any idea of more options
            _operations = new IOperation[] {
                new Operation("Write", Write)
            };
        }

        public bool StartOperation()
        {
            Console.WriteLine("Choose one of the following operations");
            int optionNumber = 1;
            foreach (var o in _operations)
            {
                Console.WriteLine($"{optionNumber++}.{o.OperationDescription}");
            }
            int chosenOperation;
            while (!int.TryParse(Console.ReadLine(), out chosenOperation) || chosenOperation == 0 || chosenOperation > _operations.Length)
            {
                Console.WriteLine("The operation doesn't exist");
            }

            Console.WriteLine("Please enter the log text");
            var logText = Console.ReadLine();


            _operations[chosenOperation - 1].Perform(logText);

            OperationWasChosen?.Invoke(this, new OperationEventArgs()
            {
                OperationName = _operations[chosenOperation - 1].OperationDescription,
                OperationLogText = logText
            });

            Console.WriteLine("Press Enter");
            Console.ReadLine();
            Console.WriteLine("End? press Y");
            var a = Console.ReadLine();
            Console.Clear();
            if (a == "y")
            {
                return isOn = false;
            }
            return true;
        }

        private void Write(string text)
        {
            Console.WriteLine("The text was logged: " + text);
        }
    }
}
