﻿namespace Operations
{
    public class OperationEventArgs
    {
        public string OperationName { get; set; }
        public string OperationLogText { get; set; }
    }
}