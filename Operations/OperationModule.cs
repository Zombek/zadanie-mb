﻿using Ninject.Modules;
using Operations.Interfaces;

namespace Operations
{
    public class OperationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IOperationManager>().To<OperationManager>().InSingletonScope();
            Bind<IOperation>().To<Operation>();
        }
    }
}
