﻿using Operations.Interfaces;
using System;

namespace Operations
{
    internal class Operation : IOperation
    {
        private Action<string> _executeOperation;
        public string OperationDescription { get; }

        public Operation(string operationDescription, Action<string> executeOperation)
        {
            OperationDescription = operationDescription;
            _executeOperation = executeOperation;
        }

        public void Perform(string a)
        {
            _executeOperation(a);
        }
    }
}