﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Operations.Interfaces
{
    public interface IOperationManager
    {
        event EventHandler<OperationEventArgs> OperationWasChosen;
        bool StartOperation();
    }
}
