﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Operations.Interfaces
{
    public interface IOperation
    {
        string OperationDescription { get; }
        void Perform(string textlog);
    }
}
