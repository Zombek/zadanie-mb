﻿using Operations;
using Operations.Interfaces;
using Microsoft.Win32;


namespace LoggersNew

{
    public class RegistryLogger : IHistoryLogger
    {
        private readonly IOperationManager _operationManager;

        public RegistryLogger(IOperationManager operationManager)
        {
            _operationManager = operationManager;
            _operationManager.OperationWasChosen += OperationWasChosen;
        }

        public void OperationWasChosen(object source, OperationEventArgs e)
        {
            RegistryKey rk = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"SOFTWARE\TESTAPPLICATION");
            rk.SetValue(e.OperationName.ToUpper(), e.OperationLogText);
        }
    }
}
