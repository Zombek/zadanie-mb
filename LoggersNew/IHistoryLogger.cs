﻿using Operations;

namespace LoggersNew

{
    public interface IHistoryLogger
    {
        void OperationWasChosen(object source, OperationEventArgs e);
    }
}
