﻿using Operations;
using Operations.Interfaces;
using System.Xml;

namespace LoggersNew
{
    public class XMLLogger : IHistoryLogger
    {

        private readonly IOperationManager _operationManager;
        private string path = "log.xml";

        public XMLLogger(IOperationManager operationManager)
        {
            _operationManager = operationManager;
            _operationManager.OperationWasChosen += OperationWasChosen;
        }
        public void OperationWasChosen(object source, OperationEventArgs e)
        {
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartElement("Log");
                writer.WriteElementString(e.OperationName.ToUpper(), e.OperationLogText);
                writer.WriteEndElement();
                writer.Flush();
            }
        }
    }
}
