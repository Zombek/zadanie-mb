﻿using Ninject.Modules;

namespace LoggersNew
{
    public class LoggerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHistoryLogger>().To<TXTLogger>();
            Bind<IHistoryLogger>().To<RegistryLogger>();
            Bind<IHistoryLogger>().To<XMLLogger>();
        }
    }
}
