﻿using System;
using System.IO;
using Operations;
using Operations.Interfaces;

namespace LoggersNew
{
    public class TXTLogger : IHistoryLogger
    {
        private readonly IOperationManager _operationManager;
        private string path = "log.txt";

        public TXTLogger(IOperationManager operationManager)
        {
            _operationManager = operationManager;
            _operationManager.OperationWasChosen += OperationWasChosen;
        }

        public void OperationWasChosen(object source, OperationEventArgs e)
        {
            using (var sw = new StreamWriter(File.Open(path, FileMode.Append, FileAccess.Write)))
            {
                    sw.WriteLine($"{DateTime.Now} The following text was logged {e.OperationName} {e.OperationLogText}");
            }
        }
    }
}
