﻿using Ninject;
using Operations;
using Operations.Interfaces;
using LoggersNew;

namespace zadanie
{
    class Program
    {
        static void Main(string[] args)
        {

            bool isOn = true;

            IKernel kernel = new StandardKernel(new LoggerModule(), new OperationModule());

            var manager = kernel.Get<IOperationManager>();
            var historyTXT = kernel.Get<TXTLogger>();
            var historyRKEY = kernel.Get<RegistryLogger>();
            var historyXML = kernel.Get<XMLLogger>();

            do
            {
                isOn = manager.StartOperation();
            } while (isOn);

        }
    }
}
